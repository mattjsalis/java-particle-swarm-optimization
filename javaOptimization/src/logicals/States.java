package logicals;

public class States {
    final double[] currentLoc;
    final double[] ballLoc;
    final double[] basketLoc;
    final boolean hasBall;

    public States(boolean hasBall, double[] currentLoc, double[] ballLoc, double[] basketLoc)
    {
        this.currentLoc = currentLoc;
        this.ballLoc = ballLoc;
        this.basketLoc = basketLoc;
        this.hasBall = hasBall;
    }
    public double distToBall()
    {
        return Math.hypot(ballLoc[1] - currentLoc[1], ballLoc[0] - currentLoc[0]);
    }
    public double distToBasket()
    {
        return Math.hypot(basketLoc[1] - currentLoc[1], basketLoc[0] - currentLoc[0]);
    }

    public double ballDistToBasket()
    {
        return Math.hypot(basketLoc[1] - ballLoc[1], basketLoc[0] - ballLoc[0]);
    }

    public boolean hasBall()
    {
        return hasBall;
    }
}
