package logicals;

import optimization.CostFunctionOutput_IF;
import optimization.CostFunction_IF;
import optimization.Parameter;
import optimization.pso.ParticleParameter;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class PolicyCostFunction<T> implements CostFunction_IF {

    public enum If
    {
        IF
    }

    public enum AnyOrAll
    {
        ANY, ALL
    }

    public enum AndOr
    {
        AND, OR, NONE
    }

    final Map<String, Integer> startIndOfBoundedParam = new HashMap<>();
    final int maxIfThen;
    final int maxAndOrStatements;
    final int maxLogicalsInUnion;
    final int maxReturnsFromIfStatement;
    final Map<String, BiPredicate<T, T>> registeredFunctions;
    final ComputableFromPolicy<T> computableFunc;

    final LinkedHashMap<String, Double[]> otherOptimizationValues;
    final Enum<? extends Object>[] enumValuesToChooseFrom;

    String[] funcNamesAsArray;
    final List<String> boundedParamKeys = new ArrayList<>();

    List<BoundedComparator<T>> bounded;

    public PolicyCostFunction(int maxIfThen, int maxAndOrStatements, int maxLogicalsInUnion,
                              int maxReturnsFromIfStatement, Map<String, BiPredicate<T, T>> registeredFunctions,
                              ComputableFromPolicy<T> computableFunc,
                              LinkedHashMap<String, Double[]> otherOptimizationValues,
                              Enum<? extends Object>[] enumValuesToChooseFrom) {
        this.maxReturnsFromIfStatement = maxReturnsFromIfStatement;
        this.registeredFunctions = registeredFunctions;
        this.maxIfThen = maxIfThen;
        this.maxAndOrStatements = maxAndOrStatements;
        this.maxLogicalsInUnion = maxLogicalsInUnion;
        this.computableFunc = computableFunc;
        this.otherOptimizationValues = otherOptimizationValues;
        this.enumValuesToChooseFrom = enumValuesToChooseFrom;
        funcNamesAsArray = new String[registeredFunctions.keySet().size()];
        funcNamesAsArray = registeredFunctions.keySet().toArray(funcNamesAsArray);
        bounded = registeredFunctions.entrySet().stream()
                .filter(f -> f.getValue() instanceof BoundedComparator)
                .peek(e -> boundedParamKeys.add(e.getKey()))
                .map(f -> (BoundedComparator<T>)f.getValue())
                .collect(Collectors.toList());
    }

    public ParticleParameter[] computeParameters()
    {
        //First param is always the amount of if-then statements to evaluate
        List<Parameter> allIfStatements = new ArrayList<>();
        Parameter numLogicalsParam = new ParticleParameter(1, maxIfThen);
        allIfStatements.add(numLogicalsParam);
        for(int i=0;i<maxIfThen;i++)
        {
            {
                Parameter ifStart = new ParticleParameter(If.values());
                Parameter numReturns = new ParticleParameter(1, maxReturnsFromIfStatement);
                List<Parameter> returns = new ArrayList<>();
                for(int iReturn=0;iReturn<maxReturnsFromIfStatement;iReturn++)
                {
                    returns.add(new ParticleParameter(enumValuesToChooseFrom));
                }
                Parameter elseReturn = new ParticleParameter("ELSE", "NONE");
                Parameter elseReturnValue = new ParticleParameter(enumValuesToChooseFrom);
                Parameter anyOrAll = new ParticleParameter(AnyOrAll.values());
                allIfStatements.add(ifStart);
                allIfStatements.add(numReturns);
                allIfStatements.addAll(returns);
                allIfStatements.add(elseReturn);
                allIfStatements.add(elseReturnValue);
                allIfStatements.add(anyOrAll);
            }
            for(int j=0;j<maxLogicalsInUnion;j++)
            {
                ParticleParameter conditional = new ParticleParameter(funcNamesAsArray);
                allIfStatements.add(conditional);
                for(BoundedComparator<T> bound : bounded)
                {
                    ParticleParameter boundsParam = new ParticleParameter(bound.minPossibleBound,
                            bound.maxPossibleBound);
                    ParticleParameter isMax = new ParticleParameter( "FALSE", "TRUE");
                    ParticleParameter isInclusive = new ParticleParameter( "FALSE", "TRUE");
                    ParticleParameter isBothRequired = new ParticleParameter( "FALSE", "TRUE");
                    allIfStatements.add(boundsParam);
                    allIfStatements.add(isMax);
                    allIfStatements.add(isInclusive);
                    allIfStatements.add(isBothRequired);
                }
            }
            //Add possible and/or
            for(int j = 0;j<maxAndOrStatements;j++)
            {
                ParticleParameter andOr = new ParticleParameter(AndOr.values());
                ParticleParameter anyOrAll = new ParticleParameter(AnyOrAll.values());
                allIfStatements.add(andOr);
                allIfStatements.add(anyOrAll);
                for(int k=0;k<maxLogicalsInUnion;k++)
                {
                    ParticleParameter conditional = new ParticleParameter(funcNamesAsArray);
                    allIfStatements.add(conditional);
                    for(BoundedComparator<T> bound : bounded)
                    {
                        ParticleParameter boundsParam = new ParticleParameter(bound.minPossibleBound,
                                bound.maxPossibleBound);
                        ParticleParameter isMax = new ParticleParameter( "FALSE", "TRUE");
                        ParticleParameter isInclusive = new ParticleParameter( "FALSE", "TRUE");
                        ParticleParameter isBothRequired = new ParticleParameter( "FALSE", "TRUE");
                        allIfStatements.add(boundsParam);
                        allIfStatements.add(isMax);
                        allIfStatements.add(isInclusive);
                        allIfStatements.add(isBothRequired);
                    }
                }
            }
            allIfStatements.add(new ParticleParameter("END_IF"));
        }
        for(Double[] otherOptValuesBounds : otherOptimizationValues.values() )
        {
            allIfStatements.add(new ParticleParameter(otherOptValuesBounds[0], otherOptValuesBounds[1]));
        }
        ParticleParameter[] allParamsArr = new ParticleParameter[allIfStatements.size()];
        allParamsArr = allIfStatements.toArray(allParamsArr);
        return allParamsArr;
    }

    public MultipleIfStatementComparator<T> definePolicyFromParameters(Iterable<Parameter> params,
                                                                        Map<String, Double> outOptionalOtherValues,
                                                                        StringBuilder optionalReadableIfStatement)
    {
        //Extract the number of if-statements
        Iterator<Parameter> iterParams = params.iterator();
        Iterator<String> keyOtherMap = this.otherOptimizationValues == null ?
                Collections.emptyIterator() : otherOptimizationValues.keySet().iterator();
        int numIfStatements = iterParams.next().currentValue.intValue();
        int countIfStatements = 0;
        List<PolicyCostFunction.AnyOrAll> anyOrAll = new ArrayList<>();
        List<PolicyCostFunction.AndOr> andOrs = new ArrayList<>();
        int currAnyOrAll = -1;
        List<List<BiPredicate<T, T>>> conditionals = new ArrayList<>();
        int[] valToReturnUponSuccessfulComparatorEvals = null;
        Integer valToReturnUponUnsuccessfulBoolEvals = null;
        List<IfStatementComparator<T>> ifStatements = new ArrayList<>();
        while(iterParams.hasNext())
        {
            Parameter param = iterParams.next();
            String decoded = null;
            if (countIfStatements == numIfStatements && keyOtherMap.hasNext()) {
                String key = keyOtherMap.next();
                outOptionalOtherValues.put(key, param.currentValue.doubleValue());
            }
            if(countIfStatements == numIfStatements)
            {
                continue;
            }
            if(param.categoricalMap != null)
            {
                decoded = param.decode(param.currentValue.intValue());
            }
            if(param.categoricalMap != null && param.categoricalMap.containsKey("IF"))
            {
                currAnyOrAll = -1;
                Parameter numReturns = iterParams.next();
                valToReturnUponSuccessfulComparatorEvals = new int[numReturns.currentValue.intValue()];
                List<Parameter> returns = new ArrayList<>();
                for(int iReturn=0;iReturn<maxReturnsFromIfStatement;iReturn++)
                {
                    Parameter possibleReturn = iterParams.next();
                    if(iReturn<numReturns.currentValue.intValue()) {
                        returns.add(possibleReturn);
                        valToReturnUponSuccessfulComparatorEvals[iReturn] = possibleReturn.currentValue.intValue();
                    }
                }
                Parameter elseReturnP = iterParams.next();
                Parameter elseReturnValueP = iterParams.next();
                String action = returns.stream()
                        .map(aReturn -> aReturn.decode(aReturn.currentValue.intValue()))
                        .collect(Collectors.joining(","));
                String elseReturn = elseReturnP.decode(elseReturnP.currentValue.intValue());
                int elseReturnValue = elseReturnValueP.currentValue.intValue();
                boolean isElse = elseReturn.equals("ELSE") && countIfStatements == numIfStatements - 1;
                if(optionalReadableIfStatement != null)
                {
                    String elseStatement = isElse ? " ELSE "
                            + elseReturnValueP.decode(elseReturnValue)
                            : "";
                    if(countIfStatements == 0) {
                        optionalReadableIfStatement.append("return ")
                                .append(action).append(elseStatement).append(": if (");
                    }
                    else {
                        optionalReadableIfStatement.append(")\nreturn ")
                                .append(action).append(elseStatement).append(": if(");
                    }
                }
                valToReturnUponUnsuccessfulBoolEvals = isElse ? elseReturnValue : null;
            }
            else if(param.categoricalMap != null && param.categoricalMap.containsKey("END_IF"))
            {
                //Only allow the last if statement to have an else branch
                valToReturnUponUnsuccessfulBoolEvals = countIfStatements == numIfStatements - 1 ?
                        valToReturnUponUnsuccessfulBoolEvals : null;
                IfStatementComparator<T> ifStatement = new IfStatementComparator<>(new ArrayList<>(anyOrAll),
                        new ArrayList<>(andOrs), new ArrayList<>(conditionals),
                        valToReturnUponSuccessfulComparatorEvals,
                        valToReturnUponUnsuccessfulBoolEvals);
                ifStatements.add(ifStatement);
                conditionals.clear();
                anyOrAll.clear();
                andOrs.clear();
                countIfStatements++;
            }
            else if(param.categoricalMap != null && param.categoricalMap.containsKey("ANY"))
            {
               if(decoded.equals("ANY"))
               {
                   anyOrAll.add(AnyOrAll.ANY);
               }
               else {
                   anyOrAll.add(AnyOrAll.ALL);
               }
                if(optionalReadableIfStatement != null)
                {
                    optionalReadableIfStatement.append(decoded).append("[");
                }
               conditionals.add(new ArrayList<>());
               currAnyOrAll++;
            }
            else if(param.categoricalMap != null && param.categoricalMap.containsKey("AND"))
            {
                AndOr andOr = null;
                if(decoded.equals("AND"))
                {
                    andOr = AndOr.AND;
                }
                else if(decoded.equals("OR"))
                {
                    andOr = AndOr.OR;
                }
                else if(decoded.equals("NONE"))
                {
                    andOr = AndOr.NONE;
                }
                andOrs.add(andOr);
                if(optionalReadableIfStatement != null)
                {
                    optionalReadableIfStatement.append("]").append(decoded);
                }
            }
            else if(param.categoricalMap != null)
            {
                //Must be a conditional statement
                BiPredicate<T, T> func = registeredFunctions.get(decoded);
                List<Parameter> collectedBoundsParams = new ArrayList<>();
                for(int iBounds=0;iBounds<4*boundedParamKeys.size();iBounds++)
                {
                    collectedBoundsParams.add(iterParams.next());
                }
                if(func instanceof BoundedComparator)
                {
                    List<Parameter> relParams = new ArrayList<>();
                    int startIndexRelParams = boundedParamKeys.indexOf(decoded)*4;
                    for(int i=startIndexRelParams;i<startIndexRelParams+4;i++)
                    {
                        relParams.add(collectedBoundsParams.get(i));
                    }
                    BoundedComparator<T> bounded = (BoundedComparator<T>) func;
                    double bound = relParams.get(0).currentValue.doubleValue();
                    boolean isBoundMaximum = relParams.get(1).currentValue.intValue() == 1;
                    boolean isInclusive = relParams.get(2).currentValue.intValue() == 1;
                    boolean isBothToSatisfy = relParams.get(3).currentValue.intValue() == 1;
                    BoundedComparator<T> newBounded = new BoundedComparator<>(bounded.computation, bound, bounded.minPossibleBound,
                            bounded.maxPossibleBound,isBoundMaximum, isInclusive, isBothToSatisfy);
                    conditionals.get(currAnyOrAll).add(newBounded);
                    if(optionalReadableIfStatement != null)
                    {
                        String operator = isBoundMaximum ?
                                isInclusive ? "<=" : "<"
                                : isInclusive ? ">=" : ">";
                        optionalReadableIfStatement.append(decoded)
                                .append(operator).append(bound).append(",");
                    }
                }
                else {
                    conditionals.get(currAnyOrAll).add(func);
                    if(optionalReadableIfStatement != null) {
                        optionalReadableIfStatement
                                .append(decoded).append(",");
                    }
                }
            }
        }
        if(optionalReadableIfStatement != null)
        {
            optionalReadableIfStatement.append("\n").append("ELSE ")
                    .append(this.enumValuesToChooseFrom[0]);
        }
        assert(ifStatements.size() == numIfStatements);
        return new MultipleIfStatementComparator<>(ifStatements);
    }

    @Override
    public CostFunctionOutput_IF evaluateCostFunction(Parameter... parameters) {
        Map<String, Double> otherParamVals = new HashMap<>();
        MultipleIfStatementComparator<T> policy = this.definePolicyFromParameters(
                Arrays.asList(parameters), otherParamVals, null);
        return computableFunc.computeFromPolicy(policy, otherParamVals);
    }
}
