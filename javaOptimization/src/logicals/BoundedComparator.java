package logicals;

import java.util.function.*;

/**
 * Used to represent a condition that is applied at boundary
 * @param <T>
 */
public class BoundedComparator<T> implements Parameterizable<T>, BiPredicate<T, T> {
    final Function<T, Double> computation;
    final boolean isInclusive;
    final boolean isBothToSatisfy;
    final Double bound;
    final boolean isBoundMaximum;

    final double minPossibleBound;

    final double maxPossibleBound;

    public void setOnlyTestLeft(boolean onlyTestLeft) {
        isOnlyTestLeft = onlyTestLeft;
    }

    public void setOnlyTestRight(boolean onlyTestRight) {
        isOnlyTestRight = onlyTestRight;
    }

    boolean isOnlyTestLeft = false;

    boolean isOnlyTestRight = false;

    public BoundedComparator(Function<T, Double> computation,
                             Double bound,
                             Double minPossibleBound,
                             Double maxPossibleBound,
                             boolean isBoundMaximum,
                             boolean isInclusive,
                             boolean isBothToSatisfy) {
        this.computation = computation;
        this.bound = bound;
        this.minPossibleBound = minPossibleBound;
        this.maxPossibleBound = maxPossibleBound;
        this.isBoundMaximum = isBoundMaximum;
        this.isInclusive = isInclusive;
        this.isBothToSatisfy = isBothToSatisfy;
    }

    @Override
    public boolean test(T o1, T o2)
    {
        if(isOnlyTestLeft)
        {
            return test(o1);
        }
        if(isOnlyTestRight)
        {
            return test(o2);
        }
        return isBothToSatisfy ? test(o1) && test(o2) : test(o1) || test(o2);
    }

    public boolean test(T o)
    {
        Double comp1 = computation.apply(o);
        boolean isGreaterThan = comp1 > bound;
        if(isInclusive)
        {
            boolean isEqual = comp1.equals(bound);
            return isEqual || !(isBoundMaximum == isGreaterThan);
        }
        return !(isBoundMaximum == isGreaterThan);
    }

    @Override
    public Parameterizable<T> parameterizeFromDouble(Double aDouble) {
        return new BoundedComparator<>(computation, aDouble, minPossibleBound, maxPossibleBound,
                isBoundMaximum, isInclusive, isBothToSatisfy);
    }
}
