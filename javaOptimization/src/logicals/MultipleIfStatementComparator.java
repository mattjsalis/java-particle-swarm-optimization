package logicals;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class MultipleIfStatementComparator<T> implements BiFunction<T, T, int[]> {

    final List<IfStatementComparator<T>> ifStatements;

    public MultipleIfStatementComparator(List<IfStatementComparator<T>> ifStatements)
    {

        this.ifStatements = ifStatements;
    }

    @Override
    public int[] apply(T o1, T o2) {
        return ifStatements.stream()
                .map(s -> s.apply(o1, o2))
                .filter(r -> !Arrays.equals(r, new int[]{0}))
                .findFirst()
                .orElse(new int[]{0});
    }
}
