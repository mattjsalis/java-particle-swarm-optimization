package logicals;

import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class IfStatementComparator<T> implements BiFunction<T, T, int[]> {
    final List<PolicyCostFunction.AnyOrAll> anyOrAll;
    final List<PolicyCostFunction.AndOr> andOrs;
    final List<List<BiPredicate<T,T>>> conditionals;
    boolean isConstainsTheAndEnum = false;
    boolean isConstainsTheOrEnum = false;
    final int[] valToReturnUponSuccessfulComparatorEvals;
    final Integer elseReturnValue;

    /**
     *
     * @param anyOrAll
     * @param andOrs
     * @param conditionals
     * @param valToReturnUponSuccessfulComparatorEvals
     */
    public IfStatementComparator(List<PolicyCostFunction.AnyOrAll> anyOrAll,
                                 List<PolicyCostFunction.AndOr> andOrs,
                                 List<List<BiPredicate<T,T>>> conditionals,
                                 int[] valToReturnUponSuccessfulComparatorEvals,
                                 Integer elseReturnValue)
    {
        this.anyOrAll = anyOrAll;
        this.andOrs = andOrs;
        this.conditionals = conditionals;
        isConstainsTheAndEnum = andOrs.contains(PolicyCostFunction.AndOr.AND);
        isConstainsTheOrEnum = andOrs.contains(PolicyCostFunction.AndOr.OR);
        this.valToReturnUponSuccessfulComparatorEvals = valToReturnUponSuccessfulComparatorEvals;
        this.elseReturnValue = elseReturnValue;
    }

    /**
     * If the provided encoded if-statement would be evaluated to true,
     * return the desired value. Otherwsie, return 0 (no preference).
     * @param o1 the first object to be compared.
     * @param o2 the second object to be compared.
     * @return
     */
    @Override
    public int[] apply(T o1, T o2) {
        //Want to split on OR statements
       boolean isSatisfied = evaluateListOfConditionals(anyOrAll.get(0),
                this.conditionals.get(0), o1, o2);
        if(isSatisfied && (!isConstainsTheAndEnum || andOrs.get(0) == PolicyCostFunction.AndOr.OR))
        {
            return valToReturnUponSuccessfulComparatorEvals;
        }
        for(int iConditionals=1;iConditionals<conditionals.size();iConditionals++)
        {
            PolicyCostFunction.AndOr andOr = andOrs.get(iConditionals-1);
            if(andOr == PolicyCostFunction.AndOr.NONE ||
                    (andOr == PolicyCostFunction.AndOr.AND && !isSatisfied))
            {
                continue;
            }
            else if (isSatisfied)
            {
                //Must be an or: Don't need to evaluate any more statements
                // because the previous was already true
                return valToReturnUponSuccessfulComparatorEvals;
            }
            else {
                //We didn't satisfy the last set of conditionals, but each
                // OR is effectively a new if-statement so set the satisfaction to
                // true
                isSatisfied = true;
            }
            PolicyCostFunction.AnyOrAll anyOrAll1 = anyOrAll.get(iConditionals);
            isSatisfied = evaluateListOfConditionals(anyOrAll1,
                    this.conditionals.get(iConditionals),
                    o1, o2);

        }
        return isSatisfied ? valToReturnUponSuccessfulComparatorEvals
                : elseReturnValue == null ? new int[]{0} : new int[]{elseReturnValue};
    }

    private boolean evaluateListOfConditionals(PolicyCostFunction.AnyOrAll anyOrAll_,
                                           List<BiPredicate<T,T>> conditionalsForEval,
                                            T o1, T o2)
    {
        boolean isAny = anyOrAll_ == PolicyCostFunction.AnyOrAll.ANY;
        boolean isFullfilledAnyOrAll = true;
        for(int i=0;i<conditionalsForEval.size();i++)
        {
            boolean isDesiredConditions = conditionalsForEval.get(i).test(o1, o2);
            if(isAny && isDesiredConditions)
            {
                break;
            }
            else if(!isAny && !isDesiredConditions)
            {
                isFullfilledAnyOrAll = false;
                break;
            }
        }
        return isFullfilledAnyOrAll;
    }

    boolean isTerminalConditional()
    {
        return elseReturnValue != null;
    }
}
