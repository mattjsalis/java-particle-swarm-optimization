package logicals;

public interface Parameterizable<T>{
    Parameterizable<T> parameterizeFromDouble(Double aDouble);
}
