package logicals;

import optimization.BestDiscoveredSolution;
import optimization.CostFunctionOutput_IF;
import optimization.pso.ParticleSwarm;

import java.util.*;
import java.util.function.BiPredicate;

public class Blackjack implements ComputableFromPolicy<List<Integer>>{

    public class BlackjackSimResult implements CostFunctionOutput_IF
    {
        final int nWon;
        final int nBusted;
        int nConditionals;
        public BlackjackSimResult(int nWon, int nBusted, int nConditionals)
        {
            this.nWon = nWon;
            this.nBusted = nBusted;
            this.nConditionals = nConditionals;
        }

        @Override
        public boolean isNewCostFunctionOutputBetter(CostFunctionOutput_IF newCFOutput) {
            BlackjackSimResult res = (BlackjackSimResult)newCFOutput;
            if(res.nWon > nWon)
            {
                return true;
            }
            else if(res.nWon < nWon)
            {
                return false;
            }
            if(res.nBusted < nBusted)
            {
                return true;
            }
            return res.nConditionals < nConditionals;
        }

        @Override
        public boolean isOptimizationCriterionSatisified() {
            return false;
        }

        @Override
        public boolean isSolutionWithinRestraints() {
            return true;
        }

        @Override
        public void printOutput() {

        }

        @Override
        public String getOutputAsString() {
            return "nWon: " + nWon + ", nBusted:" + nBusted;
        }
    }

    public enum HitOrStay
    {
        HIT, STAY
    }


    @Override
    public CostFunctionOutput_IF computeFromPolicy(MultipleIfStatementComparator<List<Integer>> policy,
                                                   Map<String, Double> otherParamVals)
    {
        Random rand = new Random(12345);
        int nBusted = 0;
        int nWon = 0;
        int nConditionals = policy.ifStatements.stream()
                .mapToInt(ifS -> ifS.conditionals.stream()
                        .mapToInt(inner -> inner.size()).sum())
                .sum();
        for(int i=0;i<5000;i++)
        {
            //Set initial probabilities
            Map<Integer, Integer> cardValueToRemaining = new HashMap<>();
            for(int v=2;v<10;v++)
            {
                cardValueToRemaining.put(v, 4);
            }
            cardValueToRemaining.put(10, 16);
            cardValueToRemaining.put(11, 4);
            //Draw hands
            int dealer1 = drawCard(rand, cardValueToRemaining);
            int dealer2 = drawCard(rand, cardValueToRemaining);
            int player1 = drawCard(rand, cardValueToRemaining);
            int player2 = drawCard(rand, cardValueToRemaining);
            int sumPlayerHand = player1 + player2;
            int sumDealerHand = dealer1 + dealer2;
            if(sumDealerHand == 21)
            {
                continue;
            }
            else if(sumPlayerHand == 21)
            {
                nWon++;
                continue;
            }
            //Player only knows about one card
            List<Integer> dealerHand = new ArrayList<>(Arrays.asList(dealer1));
            List<Integer> playerHand = new ArrayList<>(Arrays.asList(player1, player2));
            int[] hitOrStay = policy.apply(dealerHand, playerHand);
            HitOrStay hitOrStay1 = HitOrStay.values()[hitOrStay[0]];
            while(sumPlayerHand < 21 && hitOrStay1 == HitOrStay.HIT) {
                int cardVal = drawCard(rand, cardValueToRemaining);
                sumPlayerHand += cardVal;
                playerHand.add(cardVal);
                if(sumPlayerHand > 21 && playerHand.contains(11))
                {
                    playerHand.remove((Integer)11);
                    playerHand.add(1);
                    sumPlayerHand -= 10;
                }
                hitOrStay = policy.apply(dealerHand, playerHand);
                hitOrStay1 = HitOrStay.values()[hitOrStay[0]];
            }
            if(sumPlayerHand > 21)
            {
                nBusted++;
                continue;
            }
            dealerHand.add(dealer2);
            while(sumDealerHand < 17)
            {
                int newCard = drawCard(rand, cardValueToRemaining);
                if(newCard == 11 && sumDealerHand + 11 > 21)
                {
                    sumDealerHand++;
                    dealerHand.add(1);
                }
                else{
                    sumDealerHand += newCard;
                    dealerHand.add(newCard);
                }
            }
            if(sumDealerHand > 21 || sumPlayerHand > sumDealerHand)
            {
                nWon++;
            }
        }
        return new BlackjackSimResult(nWon, nBusted, nConditionals);
    }

    public int drawCard(Random rand, Map<Integer, Integer> valToRemaining)
    {
       double chance = rand.nextDouble();
       int totCards = valToRemaining.values().stream().mapToInt(v ->v).sum();
       double chanceAccum = 0.0;
       Iterator<Map.Entry<Integer, Integer>>  cardToNumLeft
               = valToRemaining.entrySet().iterator();
       while(chanceAccum < 1.0)
       {
           Map.Entry<Integer, Integer> e = cardToNumLeft.next();
           double chancePullCard = e.getValue()/(double)totCards;
           chanceAccum += chancePullCard;
           if(chanceAccum > chance)
           {
                valToRemaining.compute(e.getKey(), (k1, v) -> v-1);
                return e.getKey();
           }

       }
       return 0;
    }

    public static void main(String[] arg)
    {
        BoundedComparator<List<Integer>> sumHandDealer = new BoundedComparator<>((s) ->
                (double)s.stream().mapToInt(v->v).sum(),
                0.0, 0.0, 21.0, true,
                true, true);
        sumHandDealer.setOnlyTestLeft(true);
        BoundedComparator<List<Integer>> sumHand = new BoundedComparator<>((s) ->
                (double)s.stream().mapToInt(v->v).sum(),
                0.0, 0.0, 21.0, true,
                true, true);
        sumHand.setOnlyTestRight(true);
        BoundedComparator<List<Integer>> numAcesDealer = new BoundedComparator<>((s) ->
                (double)s.stream().filter(v -> v == 1 || v == 11).count(),
                0.0, 0.0, 4.0, true,
                true, true);
        numAcesDealer.setOnlyTestLeft(true);
        BoundedComparator<List<Integer>> numAces = new BoundedComparator<>((s) ->
                (double)s.stream().filter(v -> v == 1 || v == 11).count(),
                0.0, 0.0, 4.0, true,
                true, true);
        numAcesDealer.setOnlyTestRight(true);
        Map<String, BiPredicate<List<Integer>, List<Integer>>> logicals = new HashMap<>();
        logicals.put("sumDealerHand", sumHandDealer);
        logicals.put("sumHand", sumHand);
        logicals.put("numAcesDealer", numAcesDealer);
        logicals.put("numAces", numAces);
        LinkedHashMap<String, Double[]> actionParams = new LinkedHashMap<>();
        PolicyCostFunction<List<Integer>> policyCf = new PolicyCostFunction<>(3,
                0, 2, 1,
                logicals, new Blackjack(), actionParams, Blackjack.HitOrStay.values());
        ParticleSwarm swarm = new ParticleSwarm(30, 200,
                0.1, policyCf.computeParameters());
        BestDiscoveredSolution bestDiscoveredSolution = swarm.optimize(policyCf);
        bestDiscoveredSolution.printSolution();
        StringBuilder policy = new StringBuilder();
        Map<String, Double> otherParamEvals = new HashMap<>();
        MultipleIfStatementComparator<List<Integer>> multi = policyCf.definePolicyFromParameters(bestDiscoveredSolution.getParameters(),
                otherParamEvals,policy);
        System.out.println(policy.toString());
        (new Blackjack()).computeFromPolicy(multi,otherParamEvals );
    }

}
