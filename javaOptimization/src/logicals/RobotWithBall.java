package logicals;

import optimization.BestDiscoveredSolution;
import optimization.CostFunctionOutput_IF;
import optimization.pso.ParticleSwarm;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class RobotWithBall implements ComputableFromPolicy<States>{

    double[] ballInit = new double[]{0.0,0.0};
    double[] basket = new double[]{10.0, 10.0};
    double[] robotInit = new double[]{5.0, 5.0};

    public class RobotResult implements CostFunctionOutput_IF
    {
        boolean isPickedUpBall = false;
        double distToBallAtEnd = 0.0;
        double ballDistToGoal = 0.0;

        int nTimesTryPickUpBall = 0;

        int nTimesDidNothing = 0;
        int nDistinctActions;

        long numConditionals;
        double distballDistToBasket;
        double tGotBall;
        double tEnd;
        Integer firstTerminalConditional;

        public RobotResult(boolean isPickedUpBall, double distToBallAtEnd, double ballDistToGoal,
                           int nTimesTryPickUpBall, int nTimesDidNothing, double distballDistToBasket,
                           int nDistinctActions,long numConditionals,
                           double tGotBall, double tEnd,
                           Integer firstTerminalConditional)
        {
            this.isPickedUpBall = isPickedUpBall;
            this.ballDistToGoal = ballDistToGoal;
            this.distToBallAtEnd = distToBallAtEnd;
            this.nTimesTryPickUpBall = nTimesTryPickUpBall;
            this.nTimesDidNothing = nTimesDidNothing;
            this.distballDistToBasket = distballDistToBasket;
            this.nDistinctActions = nDistinctActions;
            this.numConditionals = numConditionals;
            this.tGotBall = tGotBall;
            this.tEnd = tEnd;
            this.firstTerminalConditional = firstTerminalConditional;
        }

        @Override
        public boolean isNewCostFunctionOutputBetter(CostFunctionOutput_IF newCFOutput) {
            RobotResult newCf = (RobotResult) newCFOutput;
            if(!isPickedUpBall && !newCf.isPickedUpBall)
            {
//                if(Math.abs(newCf.distToBallAtEnd  - distToBallAtEnd) < 1e-3)
//                {
//                    return newCf.nDistinctActions > nDistinctActions;
//                }
                return newCf.distToBallAtEnd < distToBallAtEnd;
            }
            else if(!isPickedUpBall && newCf.isPickedUpBall)
            {
                return true;
            }
            else if(isPickedUpBall && !newCf.isPickedUpBall)
            {
                return false;
            }
//            if(newCf.ballDistToGoal > 1.0 && ballDistToGoal > 1.0)
//            {
//                if(newCf.firstTerminalConditional != null && firstTerminalConditional == null)
//                {
//                  return false;
//                }
//                else if(newCf.firstTerminalConditional == null && firstTerminalConditional != null)
//                {
//                    return true;
//                }
//                else if(newCf.firstTerminalConditional != null && firstTerminalConditional != null
//                && newCf.firstTerminalConditional != firstTerminalConditional)
//                {
//                    return newCf.firstTerminalConditional > firstTerminalConditional;
//                }
//            }

            if(newCf.ballDistToGoal < 0.05 && ballDistToGoal < 0.05)
            {
                return newCf.numConditionals < numConditionals;
            }
            return newCf.ballDistToGoal < ballDistToGoal;
        }

        @Override
        public boolean isOptimizationCriterionSatisified() {
            return ballDistToGoal < 1e-3;
        }

        @Override
        public boolean isSolutionWithinRestraints() {
            return true;
        }

        @Override
        public void printOutput() {

        }

        @Override
        public String getOutputAsString() {
            return "pickedUpBall: " + isPickedUpBall
                    + ", ballDistToGoal: " + ballDistToGoal
                    + ", distToBallAtEnd: " + distToBallAtEnd;
        }
    }

    @Override
    public CostFunctionOutput_IF computeFromPolicy(MultipleIfStatementComparator<States> policy,
                                                   Map<String, Double> otherParamVals) {
        long numConditionals = policy.ifStatements.stream()
                .mapToLong(ifThen -> ifThen.conditionals.stream()
                        .mapToLong(c -> c.size())
                        .sum())
                .sum();
        boolean hasBall = false;
        States init = new States(hasBall, robotInit,ballInit, basket);
        double[] vel = new double[]{0.0,0.0};
        double[] ballVel = new double[]{0.0,0.0};
        double t = 0.0;
        double dt = 0.1;
        double tGotBall = 0.0;
        boolean isEverPickedUpBall = false;
        int nTimesTryPickUpBall = 0;
        int nTimesDidNothing = 0;
        double minBallDistToBasket = Double.MAX_VALUE;
        Set<Actions> distinctActions = new HashSet<>();
        Integer firstTerminalConditional = null;
        for(int i=0;i<policy.ifStatements.size();i++)
        {
            if(policy.ifStatements.get(i).isTerminalConditional())
            {
                firstTerminalConditional = i;
                break;
            }
        }

        while(t < 60.0)
        {
            if(distinctActions.contains(Actions.PICK_UP_BALL))
            {
                System.nanoTime();
            }
            double newPosX = init.currentLoc[0] + vel[0]*dt;
            double newPosY = init.currentLoc[1] + vel[1]*dt;
            double newPosBallX = init.ballLoc[0] + ballVel[0]*dt;
            double newPosBallY = init.ballLoc[1] + ballVel[1]*dt;
            States newStates = new States(hasBall,new double[]{newPosX, newPosY},
                    new double[]{newPosBallX, newPosBallY},
                    init.basketLoc);
            int[] actionToTake = policy.apply(init, newStates);
            List<Actions> actions = Arrays.stream(actionToTake)
                            .mapToObj(actI -> Actions.values()[actI])
                            .collect(Collectors.toList());
            distinctActions.addAll(actions);
            for(Actions action : actions) {
                switch (action) {
                    case RUN_TO_BALL -> {
                        double runSpeed = otherParamVals.get("RUN_TO_BALL");
                        double[] newvel = new double[]{newStates.ballLoc[0] - newStates.currentLoc[0],
                                newStates.ballLoc[1] - newStates.currentLoc[1]};
                        double speed = Math.hypot(newvel[1], newvel[0]);
                        if (speed != 0.0) {
                            vel[0] = newvel[0] / speed * runSpeed;
                            vel[1] = newvel[1] / speed * runSpeed;
                            double distToBall = speed;
                            if (distToBall < runSpeed * dt) {
                                vel = new double[]{0.0, 0.0};
                                newStates.currentLoc[0] = newPosBallX;
                                newStates.currentLoc[1] = newPosBallY;
                            }
                        }
                    }
                    case THROW_BALL -> {
                        if (hasBall) {
                            double runSpeed = otherParamVals.get("THROW_BALL");
                            ballVel = new double[]{newStates.basketLoc[0] - newStates.currentLoc[0],
                                    newStates.basketLoc[1] - newStates.currentLoc[1]};
                            double speed = Math.hypot(ballVel[1], ballVel[0]);
                            ballVel[0] = ballVel[0] / speed * runSpeed;
                            ballVel[1] = ballVel[1] / speed * runSpeed;
                            hasBall = false;
                        }
                    }
                    case RUN_TO_BASKET -> {
                        double runSpeed = otherParamVals.get("RUN_TO_BASKET");
                        vel = new double[]{newStates.basketLoc[0] - newStates.currentLoc[0],
                                newStates.basketLoc[1] - newStates.currentLoc[1]};
                        double speed = Math.hypot(vel[1], vel[0]);
                        vel[0] = vel[0] / speed * runSpeed;
                        vel[1] = vel[1] / speed * runSpeed;
                        if (hasBall) {
                            ballVel[0] = vel[0];
                            ballVel[1] = vel[1];
                        }
                    }
                    case PICK_UP_BALL -> {
                        nTimesTryPickUpBall++;
                        if (newStates.distToBall() < 0.25) {
                            if (!isEverPickedUpBall) {
                                tGotBall = t;
                            }
                            if (!distinctActions.contains(Actions.RUN_TO_BALL)) {
                                throw new RuntimeException("How did the robot get to the ball?");
                            }
                            hasBall = true;
                            isEverPickedUpBall = true;
                            ballVel[0] = vel[0];
                            ballVel[1] = vel[1];
                            newStates.ballLoc[0] = newStates.currentLoc[0];
                            newStates.ballLoc[1] = newStates.currentLoc[1];
                        }
                    }
                }
            }
            init = newStates;
            double distballDistToBasket = init.ballDistToBasket();
            if(minBallDistToBasket > distballDistToBasket)
            {
                minBallDistToBasket = distballDistToBasket;
            }
            if(distballDistToBasket < 0.01)
            {
                break;
            }
            t += dt;
        }

        return new RobotResult(isEverPickedUpBall, init.distToBall(), init.ballDistToBasket(),
                nTimesTryPickUpBall, nTimesDidNothing, minBallDistToBasket,
                distinctActions.size(), numConditionals, tGotBall, t,
                firstTerminalConditional);
    }

    public enum Actions
    {
        NOTHING, RUN_TO_BALL, THROW_BALL, RUN_TO_BASKET, PICK_UP_BALL
    }


    public static void main(String[] args)
    {
        BoundedComparator<States> distToBallMax = new BoundedComparator<>((s) -> s.distToBall(),
                0.0, 0.0, 30.0, true, true, true);
        BoundedComparator<States> distToBasketMax = new BoundedComparator<>((s) -> s.distToBasket(),
                0.0, 0.0, 30.0, true, true, true);
        BoundedComparator<States> distBallToBasketMin = new BoundedComparator<>((s) -> s.ballDistToBasket(),
                0.0, 0.0, 30.0, true, true, true);
        Map<String, BiPredicate<States, States>> logicals = new HashMap<>();
        logicals.put("distToBall", distToBallMax);
        logicals.put("distToBasket", distToBasketMax);
        //logicals.put("distBallToBasket", distBallToBasketMin);
        logicals.put("hasBall", (s1, s2) -> s1.hasBall());
        logicals.put("notHasBall", (s1, s2) -> !s1.hasBall());
        LinkedHashMap<String, Double[]> actionParams = new LinkedHashMap<>();
        actionParams.put(Actions.RUN_TO_BALL.toString(), new Double[]{0.1, 5.0});
        actionParams.put(Actions.RUN_TO_BASKET.toString(), new Double[]{0.1, 5.0});
        actionParams.put(Actions.THROW_BALL.toString(), new Double[]{0.1, 5.0});

        PolicyCostFunction<States> policyCf = new PolicyCostFunction<>(2,
                0, 1, 1,
                logicals, new RobotWithBall(), actionParams, Actions.values());

        ParticleSwarm swarm = new ParticleSwarm(30, 20000,
                0.1, policyCf.computeParameters());
        BestDiscoveredSolution bestDiscoveredSolution = swarm.optimize(policyCf);
        bestDiscoveredSolution.printSolution();
        StringBuilder policy = new StringBuilder();
        Map<String, Double> otherParamEvals = new HashMap<>();
        MultipleIfStatementComparator<States> multi = policyCf.definePolicyFromParameters(bestDiscoveredSolution.getParameters(),
                otherParamEvals,policy);
        System.out.println(policy.toString());
        (new RobotWithBall()).computeFromPolicy(multi,otherParamEvals );
    }
}
