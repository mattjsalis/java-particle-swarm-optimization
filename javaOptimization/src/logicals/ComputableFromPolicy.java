package logicals;

import optimization.CostFunctionOutput_IF;

import java.util.Comparator;
import java.util.Map;

public interface ComputableFromPolicy<T> {
    CostFunctionOutput_IF computeFromPolicy(MultipleIfStatementComparator<T> policy, Map<String, Double> otherParamVals);
}
